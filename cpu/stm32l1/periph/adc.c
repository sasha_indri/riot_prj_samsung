/*
<<<<<<< HEAD
 * Copyright (C) 2016 Fundacion Inria Chile
 *
 * This file is subject to the terms and conditions of the GNU Lesser General
 * Public License v2.1. See the file LICENSE in the top level directory for more
 * details.
=======
 * Copyright (C) 2014-2016 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
 */

/**
 * @ingroup     cpu_stm32l1
<<<<<<< HEAD
 * @ingroup     drivers_periph_adc
=======
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
 * @{
 *
 * @file
 * @brief       Low-level ADC driver implementation
 *
<<<<<<< HEAD
 * @author      Francisco Molina <francisco.molina@inria.cl>
 * @author      Hauke Petersen <hauke.petersen@fu-berlin.de>
 * @author      Nick v. IJzendoorn <nijzendoorn@engineering-spirit.nl>
=======
 * @author      Hauke Petersen <hauke.petersen@fu-berlin.de>
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
 *
 * @}
 */

#include "cpu.h"
#include "mutex.h"
#include "periph/adc.h"

/**
 * @brief   Maximum allowed ADC clock speed
 */
#define MAX_ADC_SPEED           (12000000U)

/**
<<<<<<< HEAD
 * @brief   ADC clock settings
 *
 * NB: with ADC_CLOCK_HIGH, Vdda should be 2.4V min
 *
 */
#define ADC_CLOCK_HIGH      (0)
#define ADC_CLOCK_MEDIUM    (ADC_CCR_ADCPRE_0)
#define ADC_CLOCK_LOW       (ADC_CCR_ADCPRE_1)

/**
 * @brief   ADC sample time, cycles
 */
#define ADC_SAMPLE_TIME_4C    (0)
#define ADC_SAMPLE_TIME_9C    (1)
#define ADC_SAMPLE_TIME_16C   (2)
#define ADC_SAMPLE_TIME_24C   (3)
#define ADC_SAMPLE_TIME_48C   (4)
#define ADC_SAMPLE_TIME_96C   (5)
#define ADC_SAMPLE_TIME_192C  (6)
#define ADC_SAMPLE_TIME_384C  (7)

/**
 * @brief   Load the ADC configuration
 */
static const adc_conf_t adc_config[] = ADC_CONFIG;
=======
 * @brief   Load the ADC configuration
 * @{
 */
#ifdef ADC_CONFIG
static const adc_conf_t adc_config[] = ADC_CONFIG;
#else
static const adc_conf_t adc_config[] = {};
#endif
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2

/**
 * @brief   Allocate locks for all three available ADC device
 *
<<<<<<< HEAD
 * All STM32l1 CPU's have single ADC device
=======
 * All STM32L1 CPUs we support so far only come with a single ADC device.
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
 */
static mutex_t lock = MUTEX_INIT;

static inline void prep(void)
{
    mutex_lock(&lock);
<<<<<<< HEAD

    /* ADC clock is always HSI clock */
    if (!(RCC->CR & RCC_CR_HSION)) {
        RCC->CR |= RCC_CR_HSION;
        /* Wait for HSI to become ready */
        while (!(RCC->CR & RCC_CR_HSION)) {}
    }

    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
=======
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
    // ADC1->CR2 |= ADC_CR2_ADON;
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
}

static inline void done(void)
{
    RCC->APB2ENR &= ~(RCC_APB2ENR_ADC1EN);
<<<<<<< HEAD
    mutex_unlock(&lock);
}

static void adc_set_sample_time(uint8_t time)
=======
    //ADC1->CR2 &= ~(ADC_CR2_ADON);

    mutex_unlock(&lock);
}

void adc_set_sample_time_on_all_channels(uint8_t time)
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
{
    uint8_t i;
    uint32_t reg32 = 0;

    for (i = 0; i <= 9; i++) {
        reg32 |= (time << (i * 3));
    }

<<<<<<< HEAD
#if !defined STM32L1XX_MD
    ADC1->SMPR0 = reg32;
#endif
=======
    ADC1->SMPR0 = reg32;
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
    ADC1->SMPR1 = reg32;
    ADC1->SMPR2 = reg32;
    ADC1->SMPR3 = reg32;
}

int adc_init(adc_t line)
{
<<<<<<< HEAD
    /* check if the line is valid */
=======
    /* make sure the given line is valid */
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
    if (line >= ADC_NUMOF) {
        return -1;
    }

<<<<<<< HEAD
    /* lock and power-on the device */
    prep();

    /* configure the pin */
    if ((adc_config[line].pin != GPIO_UNDEF))
        gpio_init_analog(adc_config[line].pin);

    /* set ADC clock prescaler */
    ADC->CCR &= ~ADC_CCR_ADCPRE;
    ADC->CCR |= ADC_CLOCK_MEDIUM;

    /* Set sample time */
    /* Min 4us needed for temperature sensor measurements */
    switch (ADC->CCR & ADC_CCR_ADCPRE) {
        case ADC_CLOCK_LOW:
            /* 4 MHz ADC clock -> 16 cycles */
            adc_set_sample_time(ADC_SAMPLE_TIME_16C);
            break;
        case ADC_CLOCK_MEDIUM:
            /* 8 MHz ADC clock -> 48 cycles */
            adc_set_sample_time(ADC_SAMPLE_TIME_48C);
            break;
        default:
            /* 16 MHz ADC clock -> 96 cycles */
            adc_set_sample_time(ADC_SAMPLE_TIME_96C);
    }

    /* check if this channel is an internal ADC channel, if so
     * enable the internal temperature and Vref */
    if (adc_config[line].chan == 16 || adc_config[line].chan == 17) {
        ADC->CCR |= ADC_CCR_TSVREFE;
    }

    /* enable the ADC module */
    ADC1->CR2 = ADC_CR2_ADON;
    /* turn off during idle phase*/
    ADC1->CR1 = ADC_CR1_PDI;

    /* free the device again */
=======
    /* lock and power on the device */
    prep();
    /* configure the pin */
    /* no need to configure GPIO for ADC channels not connected to any GPIO */
    if ((adc_config[line].pin != GPIO_UNDEF)) {
        gpio_init_analog(adc_config[line].pin);
    }

    /* Set sample time */
    adc_set_sample_time_on_all_channels(0x03 /* 25 cycles */);

    /* power off an release device for now */
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
    done();

    return 0;
}

<<<<<<< HEAD
int adc_sample(adc_t line, adc_res_t res)
{
    int sample;

    /* check if resolution is applicable */
    if ( (res != ADC_RES_6BIT) &&
         (res != ADC_RES_8BIT) &&
         (res != ADC_RES_10BIT) &&
         (res != ADC_RES_12BIT)) {
        return -1;
    }

    /* lock and power on the ADC device  */
    prep();

    /* set resolution, conversion channel and single read */
    ADC1->CR1 |= res & ADC_CR1_RES;
    ADC1->SQR1 &= ~ADC_SQR1_L;
    ADC1->SQR5 = adc_config[line].chan;

    /* wait for regulat channel to be ready*/
    while (!(ADC1->SR & ADC_SR_RCNR)) {}
    /* start conversion and wait for results */
    ADC1->CR2 |= ADC_CR2_SWSTART;
    while (!(ADC1->SR & ADC_SR_EOC)) {}
    /* finally read sample and reset the STRT bit in the status register */
    sample = (int)ADC1->DR;
    ADC1 -> SR &= ~ADC_SR_STRT;

    /* power off and unlock device again */
    done();

    return sample;
}
=======
void adc_set_regular_sequence(uint8_t length, uint8_t channel[])
{
    uint32_t fifth6 = 0;
    uint32_t fourth6 = 0;
    uint32_t third6 = 0;
    uint32_t second6 = 0;
    uint32_t first6 = 0;
    uint8_t i = 0;

    if (length > 20) {
        return;
    }

    for (i = 1; i <= length; i++) {
        if (i <= 6) {
            first6 |= (channel[i - 1] << ((i - 1) * 5));
        }
        if ((i > 6) & (i <= 12)) {
            second6 |= (channel[i - 1] << ((i - 6 - 1) * 5));
        }
        if ((i > 12) & (i <= 18)) {
            third6 |= (channel[i - 1] << ((i - 12 - 1) * 5));
        }
        if ((i > 18) & (i <= 24)) {
            fourth6 |= (channel[i - 1] << ((i - 18 - 1) * 5));
        }
        if ((i > 24) & (i <= 28)) {
            fifth6 |= (channel[i - 1] << ((i - 24 - 1) * 5));
        }
    }

    ADC1->SQR1 = fifth6 | ((length - 1) << 20);
    ADC1->SQR2 = fourth6;
    ADC1->SQR3 = third6;
    ADC1->SQR4 = second6;
    ADC1->SQR5 = first6;

}

#define CR1_CLEAR_MASK            ((uint32_t)0xFCFFFEFF)
#define CR2_CLEAR_MASK            ((uint32_t)0xC0FFF7FD)

int adc_sample(adc_t line,  adc_res_t res)
{
    int sample;

    /* lock and power on the ADC device  */
    prep();
    
    /* ADC1 CR1 Configuration */
    uint32_t tmpreg1 = ADC1->CR1;
    tmpreg1 &= CR1_CLEAR_MASK;
    tmpreg1 |= res;
    ADC1->CR1 = tmpreg1;

    /* CR2 config */
    tmpreg1 = ADC1->CR2;
    tmpreg1 &= CR2_CLEAR_MASK;

    uint32_t align = 0x00000000; /* Left alignment of data */
    uint32_t edge = 0x00000000;
    uint32_t etrig = 0x03000000;
    uint32_t continuous_conv_mode = 0;

    tmpreg1 |= (uint32_t)(align | edge | etrig | ((uint32_t)continuous_conv_mode << 1));
    ADC1->CR2 = tmpreg1;

    uint8_t channels[1] = { (uint8_t) adc_config[line].chan };
    adc_set_regular_sequence(1, channels);

    /* Enable temperature and Vref conversion */
    if ((adc_config[line].pin == GPIO_UNDEF)) {
        ADC->CCR = ADC_CCR_TSVREFE;
    }
    
    /* Enable ADC */
    ADC1->CR2 |= (uint32_t)ADC_CR2_ADON;
    
    /* Wait for ADC to become ready */
    while ((ADC1->SR & ADC_SR_ADONS) == 0);
    
    ADC1->CR2 |= ADC_CR2_EOCS;

    /* Start conversion on regular channels. */
    ADC1->CR2 |= (uint32_t)ADC_CR2_SWSTART;

    /* Wait until the end of ADC conversion */
    while ((ADC1->SR & ADC_SR_EOC) == 0);

    /* read result */
    sample = (int)ADC1->DR;
    
    /* VDD calculation based on VREFINT */
    if (adc_config[line].chan == 17) { /* VREFINT is ADC channel 17 */
        uint16_t *cal = ADC_VREFINT_CAL;
        sample = 3000 * (*cal) / sample;
    }
    
    /* Chip temperature calculation */
    if (adc_config[line].chan == 16) { /* Temperature Sensor is ADC channel 16 */
        uint16_t *cal1 = ADC_TS_CAL1;
        uint16_t *cal2 = ADC_TS_CAL2;
        sample = 80 / (*cal2 - *cal1) * (sample - *cal1) + 30;
    }

    /* Disable temperature and Vref conversion */
    ADC->CCR &= ~ADC_CCR_TSVREFE;
    
    /* unlock and power off device */
    ADC1->CR2 &= ~(ADC_CR2_ADON);
    done();

    return sample;
}
>>>>>>> df4daa3168cc0ae25efdde212a87787edb03bae2
