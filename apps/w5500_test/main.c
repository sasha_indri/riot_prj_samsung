/*

 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief        application
 *
 * @author      
 * @author      
 *
 * @}
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "w5500.h"
#include "fpga.h"
//#include "w5500_params.h"
//#define ENABLE_DEBUG  (1)
//#include "debug.h"
                            // spiBus       spiCp                spiMode        spiClk          pinRst
static w5500_m_t w5500_spi = {SPI_DEV(1), GPIO_PIN(PORT_B, 12), SPI_MODE_0, SPI_CLK_100KHZ, GPIO_PIN(PORT_B, 1)};
static net_w5500_t p_net = { //  mac_addr , ip_addr, sub_mask, gtw_addr, Port, DesIp, DesPort
    {0x00,0x16,0x36,0xDE,0x58,0xF6},\
    {192,168,0,200},\
    {255,255,255,0},\
    {192,168,1,1},\
    {0,50},\
    {192,168,0,155},\
    {0,50}
}; // порт указывать десятичным числом 
                              // spiBus       spiCp                spiMode        spiClk          intUPT
static fpga_spi_t fpga_spi = {SPI_DEV(0), GPIO_PIN(PORT_B, 6), SPI_MODE_0, SPI_CLK_100KHZ, GPIO_PIN(PORT_C, 7)};
                    //answer command  mask  result_comp
static data_test_t data  ={0,  10,   55,   1 };
static uint8_t start_status[] = "{ \"ready\": \"true\", \"status\": \"Ok\"}";
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
uint8_t find_and_send_n(uint8_t *str, uint8_t *sought_for, uint8_t len);
bool parse_json(uint8_t *string, data_test_t *result, uint8_t *lenght_s);
bool find_array(uint8_t *in_str, uint16_t in_len_array, data_test_t *out_array);
void collect_json(char *answer, uint8_t len_srt, data_test_t *in_array_str, char *out_str);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(void)
{ 
    data_test_t data_array[256];
    uint8_t len_array_struct =0;
    printf("w5500 test client to server\n\r");
    printf("fpga test \n\r");
    init_w5500(&w5500_spi, &p_net);
    delay_ms_w55(1000);
    init_fpga(&fpga_spi);
    wrire_and_read(&fpga_spi, &data);

    //stop_prog();
    delay_ms_w55(10000000);
    create_tcp_client(&w5500_spi, &p_net);
    w5500_connect_to_server(&w5500_spi);
    delay_ms_w55(10000);
    w5500_transmit_data(&w5500_spi, start_status, strlen((char *)start_status));
    uint16_t size_rx = w5500_size_RX_data(&w5500_spi);
    //stop_prog();
     printf("please send text to net >>> \n\r");
    
    //close_connect(&w5500_spi);
   // stop_prog();
    //printf("IP = %d . %d .%d .%d \n\r",p_net.ip_addr[0],p_net.ip_addr[1],p_net.ip_addr[2],p_net.ip_addr[3]);
    //printf("mac = %d . %d .%d .%d .%d .%d \n\r",p_net.mac_addr[0],p_net.mac_addr[1],p_net.mac_addr[2],p_net.mac_addr[3],p_net.mac_addr[4],p_net.mac_addr[5]);
  //  */

    while (1){
        //stop_prog();
        uint8_t buf_result[256];

        //printf("write and read data = %d", wrire_and_read(&fpga_spi, &data));
        while(w5500_size_RX_data(&w5500_spi) == 0){
        delay_ms_w55(100);
        };
        size_rx = w5500_size_RX_data(&w5500_spi);
        char str[size_rx+1];
        str[size_rx] = '\0';
        printf("size resive = %d\n\r",size_rx);
        w5500_recive_data(&w5500_spi, (uint8_t *)str, size_rx);
        printf("** string >>  %s\n\r",str);
        //
        parse_json((uint8_t *)str, data_array, &len_array_struct);
        //
        for(uint8_t t = 0; t<len_array_struct; t++){
        printf("num string = %d, command = %d, mask = %d, result_comp = %d\n\r", t, data_array[t].command, data_array[t].mask, data_array[t].result_comp);
        
        }
        printf("\n end test \n\r");

        for(uint8_t t = 0; t<len_array_struct; t++){
            wrire_and_read(&fpga_spi, &data_array[t]);
            delay_ms_w55(10000);
            uint8_t temp = read(&fpga_spi);
            if(data_array[t].result_comp == temp)
                data_array[t].result_comp = 255;
            else data_array[t].result_comp = 0;
            data_array[t].answer = temp;
        }
        delay_ms_w55(1000);
        collect_json("true",len_array_struct, data_array, (char *)buf_result);
        printf("json transmit = %s",buf_result);
        w5500_transmit_data(&w5500_spi, buf_result, strlen((char *)buf_result));
        size_rx = w5500_size_RX_data(&w5500_spi);
        printf("size resive = %d\n\r",size_rx);
        delay_ms_w55(100000);
        //close_connect(&w5500_spi);
       // stop_prog();

    }
    return 0;
}
/*
 
*/
bool parse_json(uint8_t *string, data_test_t *result, uint8_t *lenght_s){

   uint8_t command = find_and_send_n(string, (uint8_t *)"command", 2);
   uint8_t len_array = find_and_send_n(string, (uint8_t *)"long", 3);
    for(uint8_t i = 0; i < len_array; i++) {
        result[i].command = command;
    }
  bool res = find_array(string, (len_array * 2), result);
  *lenght_s = len_array;
    return res;
}

uint8_t find_and_send_n(uint8_t *str, uint8_t *sought_for, uint8_t len){ // len - длинна строки str
    uint8_t *c_temp;
    uint8_t s_temp[len+1];
    s_temp[len] = '\0'; // переменные для работы 
    c_temp = (uint8_t *)strstr((char *)str, (char *)sought_for); // поиск первого символа подстроки 
    c_temp = c_temp + strlen((char *)sought_for) + 4; // первое значение того что искали 
    memcpy(s_temp, c_temp, len); // копируем то что нашли 
    uint8_t num = (uint8_t)atoi((char *)s_temp); // парсим значение 
    printf("%s = %d\n\r",(char *)sought_for, num);

    return num; // выдаём значение 
}

bool find_array(uint8_t *in_str, uint16_t in_len_array, data_test_t *out_array){ // in_len_array - какой длинны масив 
    
        char *a_temp;
        char *end_temp;
        a_temp = strstr((char *)in_str, "[");
        end_temp = strstr((char *)in_str, "]");
        if(a_temp == (char *)in_str)
            return false;

        a_temp++;

        for(uint8_t i = 0; i < in_len_array; i++){
            uint8_t num = (uint8_t)atoi(a_temp);
            if(num < 10){
                a_temp+=3;//2
            } else 
                if(num < 100){
                    a_temp+=4;// 3
                }
                else {
                    a_temp+=5;// 4
                }
            if(i%2 == 0 || i == 0)
                out_array[i/2].mask = num;
            else 
                out_array[i/2].result_comp = num;

            if(a_temp >= end_temp)
                return false;
        }

    return true;
}

void collect_json(char *answer, uint8_t len_srt, data_test_t *in_array_str, char *out_str){

    char temp_str[80]; 
    out_str[0]='{';
    out_str[1]='\0';
    strcat(out_str,"\"answer\": ");
    sprintf(temp_str,"\"%s\", ",answer);
    strcat(out_str, temp_str);
    memset(temp_str,'\0',80);
    strcat(out_str,"\"long\": ");
    sprintf(temp_str,"\"%d\", ",len_srt);
    strcat(out_str, temp_str);
    strcat(out_str,"\"data\": ");
    strcat(out_str, "[");
    for(int i = 0;i<len_srt;i++){
        memset(temp_str,'\0',80);
        sprintf(temp_str,"%d, %d, %d",in_array_str[i].mask, in_array_str[i].result_comp, in_array_str[i].answer);
        strcat(out_str, temp_str);
        if(i+1 < len_srt)
            strcat(out_str, ", ");
        
    }
    memset(temp_str,'\0',80);
    strcat(out_str, "]}");
}