#include "w5500.h"
//----------------------------------------------------------------------------------------------
void delay_ms_w55(uint32_t ms){
		uint32_t last_time = xtimer_now_usec();
		//printf("delay start %ld\n\r",last_time);
		while ((last_time + ms) > xtimer_now_usec())
		{
		}
		//printf("delay start\n\r");
	}
//----------------------------------------------------------------------------------------------
void stop_prog(void){
		DEBUG("press the button to continue\n\r");
		while(gpio_read(GPIO_PIN(PORT_C, 13))){
			delay_ms_w55(10000);
		}
		delay_ms_w55(400000);
		DEBUG(">>> \n\r");

	}
//----------------------------------------------------------------------------------------------
void w5500_write_reg(w5500_m_t * spi_p, uint8_t bloc_select, uint8_t r_data, uint16_t reg ){
   // DEBUG("WRITE REG \n");
    uint8_t write[] = {reg >> 8, reg, (bloc_select <<3)|(0x01<<2)| (OM_FDM1 & 0x03), r_data};
    uint8_t read[4];
   // DEBUG(" write write data1 = %d, write data2 = %d, write data3 = %d, meaningful write data4 = %d \n", write[0], write[1], write[2], write[3]);
    spi_transfer_bytes(spi_p->spiBus, spi_p->spiCp, false, write, read, 4);
   // printf("read data1 = %d, read data2 = %d, read data3 = %d, read data4 = %d \n", read[0], read[1], read[2], read[3]);
}
//----------------------------------------------------------------------------------------------
void w5500_write_2_reg(w5500_m_t * spi_p, uint8_t bloc_select, uint16_t r_data, uint16_t reg ){
   // DEBUG("WRITE 2 REG \n");
    uint8_t write[] = {reg >> 8, reg, (bloc_select <<3)|(0x01<<2)| (OM_FDM2 & 0x03), (uint8_t)((r_data & 0xFF00) >> 8), (uint8_t)(r_data & 0x00FF)};
    uint8_t read[5];
    spi_transfer_bytes(spi_p->spiBus, spi_p->spiCp, false, write, read, 5);
    //DEBUG("read data1 = %d, read data2 = %d, read data3 = %d, read data4 = %d \n", read[0], read[1], read[2], read[3]);
}
//----------------------------------------------------------------------------------------------
uint8_t w5500_read_reg(w5500_m_t * spi_p, uint8_t bloc_select, uint16_t reg ){
   // DEBUG("READ REG \n");
    uint8_t read[4];
    uint8_t wbuf[] = {reg >> 8, reg, (bloc_select << 3) | (OM_FDM1 & 0x03), 0x00};
    //DEBUG(" write write data1 = %d, write data2 = %d, write data3 = %d, meaningful write data4 = %d \n", wbuf[0], wbuf[1], wbuf[2], wbuf[3]);
    spi_transfer_bytes(spi_p->spiBus, spi_p->spiCp, false, wbuf, read, 4);
    //DEBUG("read data1 = %d, read data2 = %d, read data3 = %d, meaningful read data4 = %d \n", read[0], read[1], read[2], read[3]);
    return read[3];
}
//----------------------------------------------------------------------------------------------
uint16_t w5500_read_2_reg(w5500_m_t * spi_p, uint8_t bloc_select, uint16_t reg ){
    //DEBUG("READ 2 REG \n");
    uint8_t read[5];
    uint8_t wbuf[] = {reg >> 8, reg, (bloc_select << 3) | (OM_FDM2 & 0x03), 0x00, 0x00};
    spi_transfer_bytes(spi_p->spiBus, spi_p->spiCp, false, wbuf, read, 5);
    //printf("read data1 = %d, read data2 = %d, read data3 = %d, meaningful read data4 = %d", read[0], read[1], read[2], read[3]);
    return read[3]<<8 | read[4];
}
//----------------------------------------------------------------------------------------------
void w5500_read_mass(w5500_m_t *spi_p, uint8_t bloc_select, uint16_t reg, uint8_t *read_data, size_t len_data){
   // DEBUG("READ mass \n");
    uint8_t read[3];
    uint8_t wbuf[] = {reg >> 8, reg, (bloc_select << 3)  | (OM_FDM0 & 0x03)};
    spi_transfer_bytes(spi_p->spiBus, spi_p->spiCp, true, wbuf, read, 3);
    spi_transfer_bytes(spi_p->spiBus, spi_p->spiCp, false, 0x00, read_data, len_data);
   // DEBUG(" \n read data1 = %d, read data2 = %d, read data3 = %d \n", read[0], read[1], read[2]);
}
//----------------------------------------------------------------------------------------------
void w5500_write_mass(w5500_m_t * spi_p, uint8_t bloc_select, uint8_t *write_data, uint16_t reg, size_t len_data ){
   // DEBUG("WRITE mass \n");
    uint8_t write[] = {reg >> 8, reg, (bloc_select <<3)|(0x01<<2)| (OM_FDM0 & 0x03)};
    uint8_t read[len_data+2];
    spi_transfer_bytes(spi_p->spiBus, spi_p->spiCp, true, write, read, 3);
    spi_transfer_bytes(spi_p->spiBus, spi_p->spiCp, false, write_data, read, len_data);
}
//----------------------------------------------------------------------------------------------
void initial_spi(w5500_m_t * spi_p){
    spi_init(spi_p->spiBus);
    spi_init_cs(spi_p->spiBus, spi_p->spiCp);
    spi_acquire(spi_p->spiBus, spi_p->spiCp, spi_p->spiMode, spi_p->spiClk );
    DEBUG("spi init\n\r");
}
//----------------------------------------------------------------------------------------------
uint8_t w5500_getSatatus(w5500_m_t *spi_p){
return w5500_read_reg(spi_p, BSB_S0, Sn_SR);
}
//----------------------------------------------------------------------------------------------
bool w5500_socet0_CR(w5500_m_t *spi_p, uint8_t command, uint8_t test, uint32_t timeout){
         w5500_write_reg(spi_p, BSB_S0, command, Sn_CR);
    for(int i = 0; i<=5; i++ ){
        if((test == w5500_getSatatus(spi_p)))
             return true;
        else {
             delay_ms_w55(timeout);
        }
    }
     DEBUG("status =  %d\n\r",w5500_getSatatus(spi_p));
     DEBUG("command = %d socet is fail, test = %d\n\r",command, test);
    return false;
}
//----------------------------------------------------------------------------------------------
bool w5500_OPEN_tcp(w5500_m_t *spi_p){
          w5500_write_reg(spi_p, BSB_S0, Mode_TCP, Sn_MR);
   return w5500_socet0_CR(spi_p, OPEN, SOCK_INIT, 400);
}
//----------------------------------------------------------------------------------------------
bool w5500_connect_to_server(w5500_m_t *spi_p){
    return w5500_socet0_CR(spi_p, CONNECT, SOCK_ESTABLISHED, 35000);
}
//----------------------------------------------------------------------------------------------
bool w5500_close_connect(w5500_m_t *spi_p){
    return w5500_socet0_CR(spi_p, CLOSE, SOCK_CLOSED, 400);
}
//----------------------------------------------------------------------------------------------
bool w5500_listen_connect(w5500_m_t *spi_p){
    return w5500_socet0_CR(spi_p, LISTEN, SOCK_LISTEN, 400);
}
//----------------------------------------------------------------------------------------------
bool w5500_disconect(w5500_m_t *spi_p){
    return w5500_socet0_CR(spi_p, DISCON, SOCK_CLOSED, 2500);
}
//----------------------------------------------------------------------------------------------
bool w5500_SEND(w5500_m_t *spi_p){
    return w5500_socet0_CR(spi_p, SEND, SOCK_ESTABLISHED, 400);
}
//----------------------------------------------------------------------------------------------
bool w5500_SEND_KEEP(w5500_m_t *spi_p){
    return w5500_socet0_CR(spi_p, SEND_KEEP, SOCK_ESTABLISHED, 400);
}
//----------------------------------------------------------------------------------------------
bool w5500_RECV(w5500_m_t *spi_p){
    return w5500_socet0_CR(spi_p, RECV, SOCK_ESTABLISHED, 400);
}
//----------------------------------------------------------------------------------------------
uint16_t w5500_size_RX_data(w5500_m_t *spi_p){ // количество принятых байтов 
    uint16_t temp = w5500_read_2_reg(spi_p, BSB_S0, Sn_RX_RSR0);
    //uint16_t write_pointer_RX = w5500_read_2_reg(spi_p, BSB_S0, Sn_RX_WR0); // значение последнего адреса записи [r]
    //uint16_t read_pointer_RX =  w5500_read_2_reg(spi_p, BSB_S0, Sn_RX_RD0); // значение первого записанного  [r/w]
    return temp;//write_pointer_RX - read_pointer_RX;
}
//----------------------------------------------------------------------------------------------
uint8_t w5500_event_regestration(w5500_m_t *spi_p){ // получаем регестрируем событие 
    return w5500_read_reg(spi_p, BSB_S0, Sn_IR);
}
//----------------------------------------------------------------------------------------------
bool w5500_write_tx_buf(w5500_m_t *spi_p, uint8_t *data, uint16_t linght){
  uint16_t free_size     =  w5500_read_2_reg(spi_p, BSB_S0, Sn_TX_FSR0); // свободное место 
  uint16_t read_pointer;
  uint16_t write_pointer;
  int i = 0;
  do
  {
     if((w5500_getSatatus(spi_p) == SOCK_CLOSED)){
            return false;
     }
     delay_ms_w55(1000);
     read_pointer  =  w5500_read_2_reg(spi_p, BSB_S0, Sn_TX_RD0);  // точка переданых данных
     write_pointer =  w5500_read_2_reg(spi_p, BSB_S0, Sn_TX_WR0);  // последняя точка записи 
     DEBUG("read_pointer = %d\n  write_pointer = %d\n\r",read_pointer, write_pointer);
     i++;
     if(i>=5) {
         w5500_disconect(spi_p);
         w5500_close_connect(spi_p);
         delay_ms_w55(1000);
         DEBUG("ERROR socet is fail \n\r");
         i = 0;
         return false;
     }

  } while (read_pointer < write_pointer || free_size < linght );

  w5500_write_mass(spi_p, BSB_S0_TX, data, write_pointer, linght);
  w5500_write_2_reg(spi_p, BSB_S0, write_pointer + linght, Sn_TX_WR0);
  return true;
 // взять значение текущей точки 
 // взять свободного места 
 // сравнить длинну свободного места и длинну строки 
 // начать писать значения исходя из текущей точки 
}
//----------------------------------------------------------------------------------------------
bool w5500_read_rx_buf(w5500_m_t *spi_p, uint8_t *data, uint16_t linght){
    uint16_t write_pointer_RX = w5500_read_2_reg(spi_p, BSB_S0, Sn_RX_WR0); // значение последнего адреса записи [r]
    uint16_t read_pointer_RX =  w5500_read_2_reg(spi_p, BSB_S0, Sn_RX_RD0); // значение первого записанного  [r/w]
     DEBUG("write_pointer_RX = %d\n\r",write_pointer_RX);
     DEBUG("read_pointer_RX = %d\n\r",read_pointer_RX);

    if(write_pointer_RX == read_pointer_RX)
        return false;
    w5500_read_mass(spi_p, BSB_S0_RX, read_pointer_RX, data, linght);
    w5500_write_2_reg(spi_p, BSB_S0, write_pointer_RX, Sn_RX_RD0);
    w5500_RECV(spi_p);
   // write_pointer_RX = w5500_read_2_reg(spi_p, BSB_S0, Sn_RX_WR0); // значение последнего адреса записи [r]
   // read_pointer_RX =  w5500_read_2_reg(spi_p, BSB_S0, Sn_RX_RD0); // значение первого записанного  [r/w]
    // DEBUG("write_pointer_RX = %d\n\r",write_pointer_RX);
    // DEBUG("read_pointer_RX = %d\n\r",read_pointer_RX);
    return true;
    // взять значение текущей точки  
    // сколько значений было получено 
    // начать читать 
}
//----------------------------------------------------------------------------------------------
bool compare_status(w5500_m_t *spi_p, uint8_t word){

    while(w5500_getSatatus(spi_p) != word) {
        delay_ms_w55(10);
    }
    return true;
}
//----------------------------------------------------------------------------------------------
void create_tcp_server(w5500_m_t *spi_p){
    w5500_OPEN_tcp(spi_p);
    w5500_listen_connect(spi_p);
    DEBUG(" \n status reg = %d\n\r", w5500_getSatatus(spi_p));
    DEBUG(" create_tcp_server \n\r");
}
//----------------------------------------------------------------------------------------------
void set_destination_param(w5500_m_t *spi_p, net_w5500_t *net_p){
    // прописать настройку порта удаленного 
    uint8_t temp_read[4];
    w5500_write_2_reg(spi_p, BSB_S0, (uint16_t)(net_p->DesPort[0]<<8 | net_p->DesPort[1]), Sn_DPORT0);
    DEBUG("* destination port = %d *\n\r", w5500_read_2_reg(spi_p, BSB_S0, Sn_DPORT0));
    // прописать настройку IP удаленного 
    w5500_write_mass(spi_p, BSB_S0, net_p->DesIp,Sn_DIPR0, 4);
    w5500_read_mass(spi_p, BSB_S0, Sn_DIPR0, temp_read, 4);
    DEBUG("* destination ip = %d:%d:%d:%d *\n\r", temp_read[0], temp_read[1], temp_read[2],temp_read[3]);

}
//----------------------------------------------------------------------------------------------
void create_tcp_client(w5500_m_t *spi_p, net_w5500_t *net_p){
    set_destination_param(spi_p, net_p);
    DEBUG(" \n Sn_IR = %d\n\r",w5500_read_reg(spi_p, BSB_S0, Sn_IR));
    w5500_OPEN_tcp(spi_p);
}
//----------------------------------------------------------------------------------------------
void clear_interupt(w5500_m_t *spi_p){ // <= not work=====================================
    w5500_write_reg(spi_p, BSB_COMMON, 0xFF, SIR); // очищаем 
    delay_ms_w55(1000);
    w5500_write_reg(spi_p, BSB_COMMON, 0x00, SIR); // очищаем 
}
//----------------------------------------------------------------------------------------------
void enable_interupt(w5500_m_t *spi_p){ // разрешаем прерывания 
    w5500_write_reg(spi_p, BSB_COMMON, 0xFF, SIMR); // разрешаем прерываниия на первый сокет 
    clear_interupt(spi_p);
    w5500_write_reg(spi_p, BSB_S0, (CON_IR|DISCON_IR|RECV_IR|TIMEOUT_IR|SEND_OK_IR), Sn_IMR); // инициализация маски прерывания
}
//----------------------------------------------------------------------------------------------
bool  w5500_transmit_data(w5500_m_t *spi_p, uint8_t *data, uint16_t linght){
    uint8_t status = 0;
    bool result = false;
    w5500_write_tx_buf(spi_p, data, linght);
    w5500_SEND(spi_p);
    status = w5500_event_regestration(spi_p);
    DEBUG(" \n Sn_IR = %d\n\r",status);
    result = ((status & SEND_OK_IR)> 0);
    clear_interupt(spi_p);
    return result;
}
bool  w5500_recive_data(w5500_m_t *spi_p, uint8_t *data, uint16_t linght){ 
        uint16_t size = w5500_size_RX_data(spi_p);
        if(size == 0){
            DEBUG(" \n error no data\n\r");
            return false;
        }
        if(size > linght){
            w5500_read_rx_buf(spi_p, data, linght);
            return false;
        } else {
            w5500_read_rx_buf(spi_p, data, size);
        }

        
return false; 
}
//----------------------------------------------------------------------------------------------
void init_w5500(w5500_m_t *spi_p, net_w5500_t *net_p){
    uint8_t read_d[6];
    xtimer_init();
	gpio_init(GPIO_PIN(PORT_C, 13), GPIO_IN_PU);
    gpio_init(spi_p->pinRst, GPIO_OUT);
    delay_ms_w55(1000);
////////////////////////////////////////////////////
    gpio_set(spi_p->pinRst); // reset 
    gpio_clear(spi_p->pinRst);
    delay_ms_w55(100);
    gpio_set(spi_p->pinRst);
    delay_ms_w55(100);
    gpio_clear(spi_p->pinRst);
    delay_ms_w55(100);
    gpio_set(spi_p->pinRst);
    delay_ms_w55(1000);
////////////////////////////////////////////////////
    initial_spi(spi_p); 
    w5500_write_reg(spi_p, BSB_COMMON, MODE_RESET, REG_MODE);
    delay_ms_w55(100);
    printf("status reg = %d\n\r", w5500_getSatatus(spi_p));
	//stop_prog();

    w5500_write_mass(spi_p, BSB_COMMON, net_p->gtw_addr, GWR0, 4);
    w5500_write_mass(spi_p, BSB_COMMON, net_p->sub_mask, SUBR0, 4);
    w5500_write_mass(spi_p, BSB_COMMON, net_p->mac_addr, SHAR0, 6);
    w5500_write_mass(spi_p, BSB_COMMON, net_p->ip_addr, SIPR0, 4);
    printf(" \n status reg = %d\n\r", w5500_getSatatus(spi_p));
/////////////////////////////////////////////////////////////////////////////////////////////
   
    w5500_read_mass(spi_p, BSB_COMMON, GWR0, read_d, 4);
    printf("\n\r gtw_addr >> **\n\r GWR0 = %d:%d:%d:%d \n\r",read_d[0], read_d[1], read_d[2], read_d[3]);

    w5500_read_mass(spi_p, BSB_COMMON, SIPR0, read_d, 4);
    printf("\n\r ip_addr >> **\n\r SIPR0 = %d:%d:%d:%d \n\r",read_d[0], read_d[1], read_d[2], read_d[3]);

    w5500_read_mass(spi_p, BSB_COMMON, SUBR0, read_d, 4);
    printf("\n\r sub_mask >> **\n\r SUBR0 = %d:%d:%d:%d \n\r",read_d[0], read_d[1], read_d[2], read_d[3]);

    w5500_read_mass(spi_p, BSB_COMMON, SHAR0, read_d, 6);
    printf("\n\r mac_addr\n\r SHAR0 = %d:%d:%d:%d:%d:%d \n\r",read_d[0], read_d[1], read_d[2], read_d[3], read_d[4], read_d[5]);
    
/////////////////////////////////////////////////////////////////////////////////////////////
    delay_ms_w55(100);
    w5500_write_2_reg(spi_p, BSB_COMMON, 400, RTR0); // timeout init 400 ms
    printf("timeout = %d \n\r", w5500_read_2_reg(spi_p, BSB_COMMON, RTR0));

        // размер буферов  
    w5500_write_reg(spi_p, BSB_S0, 4, Sn_RXBUF_SIZE); // 4k 
    w5500_write_reg(spi_p, BSB_S0, 4, Sn_TXBUF_SIZE);

    enable_interupt(spi_p);

    /////////////////////////////////////////////////////////////////////////////
    w5500_write_reg(spi_p, BSB_S0, 0x00, Sn_MR);
    w5500_write_2_reg(spi_p, BSB_S0, (uint16_t)((net_p->Port[0]<<8)|net_p->Port[1]), Sn_PORT0);
    /////////////////////////////////////////////////////////////////////////////
    //create_tcp_server(spi_p);  // открыть соединение к серверу или клиенту 
    //create_tcp_client(spi_p, net_p);

    //w5500_connect_to_server(spi_p); // подсоединится к серверу 

    printf(" \n status reg = %d\n\r", w5500_getSatatus(spi_p));
    printf(" \n Sn_IR = %d\n\r",w5500_read_reg(spi_p, BSB_S0, Sn_IR));
    //stop_prog();
    //w5500_transmit_data(spi_p, (uint8_t *)("{status : start, bits: 26}"), 26);

   // uint16_t size_rx = w5500_size_RX_data(spi_p);
   // stop_prog();
   // size_rx = w5500_size_RX_data(spi_p);
   // char str[size_rx+1];
    //str[size_rx] = '\0';
   // printf("size resive = %d\n\r",size_rx);
    //while(w5500_size_RX_data(spi_p) == 0){};
   // w5500_recive_data(spi_p, (uint8_t *)str, size_rx);
    //printf("\n string >>  %s\n\r",str);
    //stop_prog();
   // w5500_disconect(spi_p);
   // w5500_close_connect(spi_p);
    //w5500_write_reg(spi_p, BSB_S0, CLOSE, Sn_CR);
    delay_ms_w55(1000);
   // printf(" \n status reg = %d\n\r", w5500_getSatatus(spi_p));
    //printf(" \n Sn_IR = %d\n\r",w5500_event_regestration(spi_p));


}
void creat_conect(w5500_m_t *spi_p){
    w5500_OPEN_tcp(spi_p);
    //stop_prog();
    w5500_connect_to_server(spi_p);
    //stop_prog();
     printf("status reg = %d\n\r", w5500_getSatatus(spi_p));
}
void close_connect(w5500_m_t *spi_p){
        w5500_disconect(spi_p);
        //stop_prog();
        w5500_close_connect(spi_p);
        //stop_prog();
    printf("status reg = %d\n\r", w5500_getSatatus(spi_p));
}
/*
bool send_data_to_server(w5500_m_t *spi_p, uint8_t *data, uint16_t linght){

    return false;
}
bool get_data_from_server(w5500_m_t *spi_p, uint8_t *data, uint16_t linght_recive){

    return false;
} */
// основная идея 
// инициализация 
// установка соединения 
// заполнение буфера 
// команда отправки на адресс данных 
// чтение принятых данных 