/*
 * Copyright (C) 2019
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup     drivers_w5500
 * @{
 *
 * @file
 * @brief       Default parameters for W5500 Ethernet devices
 *
 * @author      
 */

#ifndef W5100_PARAMS_H
#define W5100_PARAMS_H

#include "board.h"
#include "periph/spi.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @name    Configuration parameters for the W5500 driver
 * @{
 */

typedef  struct 
{
    spi_t       spiBus;
    spi_cs_t    spiCp;
    spi_mode_t  spiMode;
    spi_clk_t   spiClk;
    gpio_t      pinRst;
} w5500_m_t;

typedef struct 
{
    uint8_t mac_addr[6];
    uint8_t ip_addr[4];
    uint8_t sub_mask[4];
    uint8_t gtw_addr[4];
    uint8_t Port[2];
    uint8_t DesIp[4];
    uint8_t DesPort[2];
} net_w5500_t;



#ifdef __cplusplus
}
#endif

#endif /* W5500_PARAMS_H */

