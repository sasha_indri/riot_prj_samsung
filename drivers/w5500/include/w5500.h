#ifndef W5500_H_
#define W5500_H_
//--------------------------------------------------

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#define ENABLE_DEBUG  (1)
#include "debug.h"
//#include "periph/spi.h"
#include "periph/gpio.h"
#include "xtimer.h"
#include "w5500_params.h"
#include "w5500_regs.h"

//--------------------------------------------------
#define BSB_COMMON 0x00
#define BSB_S0 0x01
#define BSB_S0_TX 0x02
#define BSB_S0_RX 0x03
//--------------------------------------------------
#define RWB_WRITE 1
#define RWB_READ 0
//--------------------------------------------------
#define OM_FDM0 0x00//����� �������� ������ ���������� �����
#define OM_FDM1 0x01//����� �������� ������ �� ������ �����
#define OM_FDM2 0x02//����� �������� ������ �� ��� �����
#define OM_FDM3 0x03//����� �������� ������ �� ������ �����
//--------------------------------------------------
#define MR 0x0000//Mode Register
//--------------------------------------------------
#define SHAR0 0x0009//Source Hardware Address Register MSB
#define SHAR1 0x000A
#define SHAR2 0x000B
#define SHAR3 0x000C
#define SHAR4 0x000D
#define SHAR5 0x000E// LSB
#define GWR0 0x0001//Gateway IP Address Register MSB
#define GWR1 0x0002
#define GWR2 0x0003
#define GWR3 0x0004// LSB
#define SUBR0 0x0005//Subnet Mask Register MSB
#define SUBR1 0x0006
#define SUBR2 0x0007
#define SUBR3 0x0008// LSB
#define SIPR0 0x000F//Source IP Address Register MSB
#define SIPR1 0x0010
#define SIPR2 0x0011
#define SIPR3 0x0012// LSB

#define RTR0  0x0019
#define RTR1  0x001A
//--------------------------------------------------
#define SIR  0x0017
#define SIMR  0x0018
//--------------------------------------------------
//--------------------------------------------------
#define Sn_PORT0 0x0004 // Socket 0 Source Port Register MSB
#define Sn_PORT1 0x0005 // Socket 0 Source Port Register LSB
//--------------------------------------------------
#define Sn_DPORT0 0x0010 // Socket 0 Destination Port MSB
#define Sn_DPORT1 0x0011 // Socket 0 Destination Port LSB
//--------------------------------------------------
#define Sn_DIPR0  0x000C // Socket 0 Destination IP Address Register
#define Sn_DIPR1  0x000D // Socket 0 Destination IP Address Register
#define Sn_DIPR2  0x000E // Socket 0 Destination IP Address Register
#define Sn_DIPR3  0x000F // Socket 0 Destination IP Address Register
//--------------------------------------------------
#define Sn_MR 0x0000 // Socket 0 Mode Register
#define Sn_CR 0x0001 // Socket 0 Command Register
#define Sn_SR 0x0003 // Socket 0 Status Register
//--------------------------------------------------
// size TX , RX 
#define Sn_RXBUF_SIZE   0x001E // Socket n RX Buffer Size Register
#define Sn_TXBUF_SIZE   0x001F // Socket n TX Buffer Size Register 
#define Sn_TX_FSR0      0x0020 // Socket n TX Free Size Register
#define Sn_TX_FSR1      0x0021
#define Sn_TX_RD0       0x0022 // Socket n TX Read Pointer Register 
#define Sn_TX_RD1       0x0023 
#define Sn_TX_WR0       0x0024 // Socket n TX Write Pointer Register
#define Sn_TX_WR1       0x0025
#define Sn_RX_RSR0      0x0026 // Socket n Received Size Register
#define Sn_RX_RSR1      0x0027
#define Sn_RX_RD0       0x0028 // Socket n RX Read Data Pointer Register
#define Sn_RX_RD1       0x0029
#define Sn_RX_WR0       0x002A // Socket n RX Write Pointer Register
#define Sn_RX_WR1       0x002B
// -------------------------------------------------
#define MODE_RESET          (0x80)  /**< device mode: reset */
#define MODE_PING           (0x10) // ENABLE PING
#define MODE_FARP           (0x02) // ENABLE ARP
//--------------------------------------------------
//Socket mode
#define Mode_CLOSED 0x00
#define Mode_TCP 0x01
#define Mode_UDP 0x02
#define Mode_MACRAV 0x04
//--------------------------------------------------
//Socket states
#define SOCK_CLOSED 0x00
#define SOCK_INIT 0x13
#define SOCK_LISTEN 0x14
#define SOCK_ESTABLISHED 0x17
#define SOCK_CLOSE_WAIT  0x1C
//-------------------------------------------
// Sn_CR (Socket n Command Register)
#define OPEN        0x01 
#define LISTEN      0x02
#define CONNECT     0x04 
#define DISCON      0x08
#define CLOSE       0x10
#define SEND        0x20
#define SEND_MAC    0x21
#define SEND_KEEP   0x22 
#define RECV        0x40
//-------------------------------------------
#define Sn_MSSR0 0x0012
#define Sn_MSSR1 0x0013
#define Sn_TX_FSR0 0x0020
#define Sn_TX_FSR1 0x0021
#define Sn_TX_RD0 0x0022
#define Sn_TX_RD1 0x0023
#define Sn_TX_WR0 0x0024
#define Sn_TX_WR1 0x0025
#define Sn_RX_RSR0 0x0026
#define Sn_RX_RSR1 0x0027
#define Sn_RX_RD0 0x0028
#define Sn_RX_RD1 0x0029
//--------------------------------------------------
#define Sn_IMR 0x002C // Socket n Interrupt Mask Register
#define Sn_IR  0x0002 // Interrupt  Register
// 
#define CON_IR     0x01 //  This is issued one time when the connection with peer is successful and then Sn_SR is changed to SOCK_ESTABLISHED.
#define DISCON_IR   0x02 //  This is issued when FIN or FIN/ACK packet is received from a peer.
#define RECV_IR     0x04 //  This is issued whenever data is received from a peer.
#define TIMEOUT_IR  0x08 //  This is issued when ARPTO or TCPTO occurs.
#define SEND_OK_IR  0x10 //  This is issued when SEND command is completed.
//--------------------------------------------------
//������� �������� ������
#define DATA_COMPLETED 0 //�������� ������ ���������
#define DATA_ONE 1 //������� ������������ �����
#define DATA_FIRST 2 //������� ������ �����
#define DATA_MIDDLE 3 //������� ������� �����
#define DATA_LAST 4 //������� ��������� �����
#define DATA_END 5 //��������� ���������� ����� �������� ������
//--------------------------------------------------
//�������� ���������� TCP
#define PRT_TCP_UNCNOWN 0
#define PRT_TCP_HTTP 1
//--------------------------------------------------
void delay_ms_w55(uint32_t ms);
void stop_prog(void);
void w5500_write_reg(w5500_m_t * spi_p, uint8_t bloc_select, uint8_t r_data, uint16_t reg );
uint8_t w5500_read_reg(w5500_m_t * spi_p, uint8_t bloc_select, uint16_t reg );
uint16_t w5500_read_2_reg(w5500_m_t * spi_p, uint8_t bloc_select, uint16_t reg );
void w5500_write_2_reg(w5500_m_t * spi_p, uint8_t bloc_select, uint16_t r_data, uint16_t reg );
void w5500_read_mass(w5500_m_t *spi_p, uint8_t bloc_select, uint16_t reg, uint8_t *read_data, size_t len_data);
void w5500_write_mass(w5500_m_t * spi_p, uint8_t bloc_select, uint8_t *write_data, uint16_t reg, size_t len_data );
void initial_spi(w5500_m_t * spi_p);
void init_w5500(w5500_m_t *spi_p, net_w5500_t *net_p);
uint8_t w5500_getSatatus(w5500_m_t *spi_p);

bool w5500_OPEN_tcp(w5500_m_t *spi_p);
bool w5500_connect_to_server(w5500_m_t *spi_p);
bool w5500_close_connect(w5500_m_t *spi_p);
bool w5500_listen_connect(w5500_m_t *spi_p);
bool w5500_disconect(w5500_m_t *spi_p);
bool w5500_SEND(w5500_m_t *spi_p);
bool w5500_SEND_KEEP(w5500_m_t *spi_p);
bool w5500_RECV(w5500_m_t *spi_p);
uint16_t w5500_size_RX_data(w5500_m_t *spi_p); 
uint8_t w5500_event_regestration(w5500_m_t *spi_p);
bool w5500_write_tx_buf(w5500_m_t *spi_p, uint8_t *data, uint16_t linght);
bool w5500_read_rx_buf(w5500_m_t *spi_p, uint8_t *data, uint16_t linght);
void creat_conect(w5500_m_t *spi_p);
void close_connect(w5500_m_t *spi_p);
void create_tcp_server(w5500_m_t *spi_p);
void set_destination_param(w5500_m_t *spi_p, net_w5500_t *net_p);
bool w5500_socet0_CR(w5500_m_t *spi_p, uint8_t command, uint8_t test, uint32_t timeout);
void create_tcp_client(w5500_m_t *spi_p, net_w5500_t *net_p);
void enable_interupt(w5500_m_t *spi_p);
void clear_interupt(w5500_m_t *spi_p);
bool w5500_recive_data(w5500_m_t *spi_p, uint8_t *data, uint16_t linght);
uint16_t w5500_size_RX_data(w5500_m_t *spi_p);
bool  w5500_transmit_data(w5500_m_t *spi_p, uint8_t *data, uint16_t linght);

//--------------------------------------------------

//--------------------------------------------------
#endif /* W5500_H_ */
