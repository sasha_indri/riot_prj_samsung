/*
 * Copyright (C) 
 *
 */

/**
 * @ingroup     
 * @{
 *
 * @file
 * @brief       Register definitions for W5100 devices
 *
 * @author      
 */

#ifndef FPGA_H
#define FPGA_H

#include "board.h"
#include "periph/spi.h"
#include "periph/gpio.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct 
{
    spi_t       spiBus;
    spi_cs_t    spiCp;
    spi_mode_t  spiMode;
    spi_clk_t   spiClk;
    gpio_t      intUpt;
}fpga_spi_t;

typedef struct 
{
    uint8_t answer;
    uint8_t command;
    uint8_t mask;
    uint8_t result_comp;

}data_test_t;

void init_fpga(fpga_spi_t *spi);
uint8_t read(fpga_spi_t *spi);
uint8_t wrire_and_read(fpga_spi_t *spi, data_test_t *data);
bool read_recive_data(uint8_t *data);

#ifdef __cplusplus
}
#endif

#endif /* W5100_REGS_H */
/** @} */
