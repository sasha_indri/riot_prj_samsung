/*
 * 
 */

/**
 * @ingroup     drivers_
 * @{
 *
 * @file
 * @brief       
 *
 * @author      
 *
 * @}
 */

#include <fpga.h>

#define ENABLE_DEBUG        (1)
#include "debug.h"
fpga_spi_t  fpga_spi_g;

uint8_t     mass_of_data[256];
uint8_t     read_count = 0;
uint8_t     rw_counter = 0;

/*
static void interupt_f(void *arg) {
	(void)arg;

	DEBUG(" result is ready, wakeup \n\r");
    
    mass_of_data[read_count] = read(&fpga_spi_g);
    read_count++;

    // read in fpga result 
}
*/
void init_fpga(fpga_spi_t *spi){
    gpio_init(spi->intUpt, GPIO_IN);
    //gpio_init_int(spi->intUpt, GPIO_IN_PD, GPIO_FALLING, interupt_f, NULL);
    DEBUG("interupt init\n\r");
    spi_init(spi->spiBus);
    spi_init_cs(spi->spiBus, spi->spiCp);
    spi_acquire(spi->spiBus, spi->spiCp, spi->spiMode, spi->spiClk );
    fpga_spi_g.spiBus   = spi->spiBus;
    fpga_spi_g.spiClk   = spi->spiClk; 
    fpga_spi_g.spiMode  = spi->spiMode;
    fpga_spi_g.spiCp    = spi->spiCp;
    fpga_spi_g.intUpt   = spi->intUpt;
    DEBUG("spi init\n\r");
}

uint8_t read(fpga_spi_t *spi){
    uint8_t read_d[1];
    
        DEBUG("pin = %d\n\r", gpio_read(spi->intUpt));

    spi_transfer_bytes(spi->spiBus, spi->spiCp, false, 0, read_d, 1);
    DEBUG("read data = %d\n\r",read_d[0]);
    
return read_d[0];
}

uint8_t wrire_and_read(fpga_spi_t *spi, data_test_t *data){
    uint8_t wrire[4] ={data->mask, data->mask, data->mask, data->command};
    uint8_t read_d[3];
    spi_transfer_bytes(spi->spiBus, spi->spiCp, false, wrire, read_d, 4);
    DEBUG("read data[0] = %d, read data[1] = %d, read data[2] = %d \n\r", read_d[0], read_d[1], read_d[2]);
return read_d[0];
}

bool read_recive_data(uint8_t *data){

    if((read_count - rw_counter) !=0) {
        *data = mass_of_data[rw_counter];
        rw_counter++;
        return true;
    }
return false;

}